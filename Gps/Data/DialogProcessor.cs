﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data
{
    public class DialogProcessor
    {
        public Graph graph = new Graph();
        List<Node> Nodes = new List<Node>();
        List<Link> Links = new List<Link>();

        public void DrawNode(Node node)
        {
            if (!graph.graph.ContainsKey(node.Name))
            {
                graph.InsertNode(node);
                Nodes.Add(node);
            }
            else MessageBox.Show("Node you entered is already in the graph :(");

        }

        public void Translate()
        {

        }

        public void CreatePath(string from, string to, double length)
        {
            Node From = null;
            Node To = null;

            if (graph.graph.ContainsKey(from) && graph.graph.ContainsKey(to))
            {
                From = graph.graph[from];
                To = graph.graph[to];
            }
            else
            {
                MessageBox.Show("Some nodes you entered are not in the graph :(");
                return;
            }
            graph.CreatePath(From, To, length);
            Links.Add(graph.graph[From.Name].paths.FirstOrDefault(p => p.EndPoint == To));
        }

        public void CreateTwoWayPath(bool IsTwoWay, string from, string to, double length)
        {
            Node From = null;
            Node To = null;

            if (graph.graph.ContainsKey(from) && graph.graph.ContainsKey(to))
            {
                From = graph.graph[from];
                To = graph.graph[to];
            }
            else
            {
                MessageBox.Show("Some nodes you entered are not in the graph :(");
                return;
            }
            if (IsTwoWay)
            {
                graph.CreateTwoWayPath(From, To, length);
                Links.Add(graph.graph[To.Name].paths.FirstOrDefault(p => p.EndPoint == From));
            }
        }

        public void ReDraw(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            Draw(e.Graphics);
        }
        public virtual void Draw(Graphics grfx)
        {
            foreach (Node item in Nodes)
            {
                item.DrawSelf(grfx);
            }
            foreach (Link item in Links)
            {
                item.DrawSelf(grfx);
            }
        }

        public void SaveAs(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, graph);
            bf.Serialize(fs, Nodes);
            bf.Serialize(fs, Links);
            fs.Close();
        }
        public void OpenFile(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            graph = (Graph)bf.Deserialize(fs);
            Nodes = (List<Node>)bf.Deserialize(fs);
            Links = (List<Link>)bf.Deserialize(fs);

            fs.Close();
        }
    }
}
