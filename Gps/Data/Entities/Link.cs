﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Data.Entities
{
    [Serializable]
    public class Link
    {
        public double Length { get; set; }
        public Node EndPoint { get; set; }
        bool IsTwoWay { get; set; }
        public LinkTypes LinkType { get; set; }

        public Node StartPoint { get; set; }

        public Link(Node from, double length, Node to, bool? isTwoWay = false)
        {
            Length = length;
            EndPoint = to;
            IsTwoWay = isTwoWay.Value;
            StartPoint = from;
        }

        public enum LinkTypes
        {
            ROAD, HIGHWAY,
        }
        public void DrawSelf(Graphics grfx)
        {

            using (Pen p = new Pen(Brushes.Blue, 3))
            {

                p.EndCap = LineCap.ArrowAnchor;

                if (IsTwoWay)
                {
                    p.StartCap = LineCap.ArrowAnchor;
                }
                grfx.DrawString(Length.ToString(), new Font("Arial", 12, FontStyle.Bold, GraphicsUnit.Point), Brushes.Black, StartPoint.X + (EndPoint.X - StartPoint.X) / 2, StartPoint.Y + (EndPoint.Y - StartPoint.Y) / 2);
                grfx.DrawLine(p, StartPoint.X, StartPoint.Y, EndPoint.X, EndPoint.Y);
            }

        }
    }
}