﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Data.Entities
{
    [Serializable]
    public class Graph
    {
        public Dictionary<string, Node> graph = new Dictionary<string, Node>();

        public void InsertNode(Node n)
        {
            if (!graph.ContainsKey(n.Name) && !graph.ContainsValue(n))
                graph.Add(n.Name, n);
        }

        public void CreatePath(Node from, Node to, double length, bool IsTwoWay=false)
        {
            if (graph.ContainsKey(from.Name) && graph.ContainsKey(to.Name))
            {
                Link path = new Link(from, length, to);
                from.InsertPath(path);

                if (IsTwoWay)
                {
                    Link pathBack = new Link(to, length, from, true);
                    to.InsertPath(pathBack);
                }

            }
            else
                Console.WriteLine("Node(s) are not part of the graph!");
        }

        public void CreateTwoWayPath(Node from, Node to, double length)
        {
            CreatePath(from, to, length, true);
        }

        //public bool Remove(Node node)
        //{
        //    if (!graph.ContainsKey(node.Name) && !graph.ContainsValue(node))
        //        graph.Remove(node.Name);

        //    // enumerate through each node in the nodeSet, removing edges to this node
        //    foreach (Link l in node.paths)
        //    {
        //        int index = gnode.Neighbors.IndexOf(nodeToRemove);
        //        if (index != -1)
        //        {
        //            // remove the reference to the node and associated cost
        //            gnode.Neighbors.RemoveAt(index);
        //            gnode.Costs.RemoveAt(index);
        //        }
        //    }

        //    return true;
        //}

        private double GetDistance(Node first, Node second)
        {
            return Math.Sqrt(Math.Pow(first.X - second.X, 2) + Math.Pow(first.Y - second.Y, 2));


        }
    }
}
