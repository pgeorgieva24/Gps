﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data.Entities
{
    [Serializable]
    public class Node
    {

        public string Name { get; set; }
        public double Weight { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool IsTested { get; set; }
        public double Distance { get; set; }

        public List<Link> paths = new List<Link>();

        public Node(string name)
        {
            Name = name;
        }

        public Node(string name, double weight)
        {
            Name = name;
            Weight = weight;
        }

        public Node(string name, int x, int y, double weight)
        {
            Name = name;
            Weight = weight;
            X = x;
            Y = y;
        }

        public void InsertPath(Link path)
        {
            paths.Add(path);
        }

        public void DrawSelf(Graphics grfx)
        {
            grfx.DrawEllipse(new Pen(Color.Black), X - 20, Y - 20, 40, 40);
            grfx.DrawString(Name, new Font("Arial", 12, FontStyle.Bold, GraphicsUnit.Point), Brushes.Black, X - 8, Y - 8);

        }
    }
}
