﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class GreedySearchAlgorithm
    {
        static int x, y;
        public static List<string> path;
        public static double distance;
        public static bool Search(string start, string finish, Graph graph)
        {
            distance = 0;
            Graph newGraph = graph;
            path = new List<string>();
            if (!newGraph.graph.ContainsKey(start) || !newGraph.graph.ContainsKey(finish))
                return false;

            List<Node> queue = new List<Node>();
            Node begining = newGraph.graph[start];
            x = newGraph.graph[finish].X;
            y = newGraph.graph[finish].Y;

            Dictionary<string, string> testedNodes = new Dictionary<string, string>();

            foreach (var item in newGraph.graph.Values)
            {
                item.IsTested = false;
            }
            queue.Add(begining);



            Node current = begining;
            Node shortest;
            double minLength = double.PositiveInfinity;

            while (queue.Count() > 0)
            {
                shortest = null;
                current = queue.ElementAt(0);
                if (current == null)
                {
                    return false;
                }
                queue.Remove(current);

                current.IsTested = true;
                path.Add(current.Name);

                if (current == newGraph.graph[finish])
                {
                    Console.WriteLine(distance);
                    return true;
                }


                foreach (var p in current.paths)
                {
                    if (!p.EndPoint.IsTested && !queue.Contains(p.EndPoint))
                    {
                        p.EndPoint.Distance = p.Length;
                        if (minLength == p.EndPoint.Distance)
                        {
                            shortest = GetClosestToEndPoint(shortest, p.EndPoint);
                        }
                        else if (minLength > p.EndPoint.Distance)
                        {
                            minLength = p.EndPoint.Distance;
                            shortest = p.EndPoint;
                        }
                    }
                }
                queue.Add(shortest);
                distance += minLength;
                minLength = double.PositiveInfinity;

                current = queue[0];

            }

            return false;
        }

        private static Node GetClosestToEndPoint(Node first, Node second)
        {
            var firstPointDistanceToEnd = Math.Sqrt(Math.Pow(first.X - x, 2) + Math.Pow(first.Y - y, 2));
            var secondPointDistanceToEnd = Math.Sqrt(Math.Pow(second.X - x, 2) + Math.Pow(second.Y - y, 2));

            if (firstPointDistanceToEnd > secondPointDistanceToEnd)
                return second;
            else return first;

        }
    }
}
