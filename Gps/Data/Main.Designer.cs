﻿namespace Data
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ViewPort = new System.Windows.Forms.Panel();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbWeight = new System.Windows.Forms.TextBox();
            this.tbX = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.Weight = new System.Windows.Forms.Label();
            this.lblX = new System.Windows.Forms.Label();
            this.tbY = new System.Windows.Forms.TextBox();
            this.lblY = new System.Windows.Forms.Label();
            this.tbStart = new System.Windows.Forms.TextBox();
            this.tbEnd = new System.Windows.Forms.TextBox();
            this.blbFrom = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.btnGreedy = new System.Windows.Forms.Button();
            this.btnShortestPath = new System.Windows.Forms.Button();
            this.lblTo2nd = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblEnd = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Path = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pathFrom = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pathTo = new System.Windows.Forms.TextBox();
            this.isTwoWay = new System.Windows.Forms.CheckBox();
            this.btnCreatePath = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ViewPort
            // 
            this.ViewPort.Location = new System.Drawing.Point(263, 49);
            this.ViewPort.Name = "ViewPort";
            this.ViewPort.Size = new System.Drawing.Size(684, 362);
            this.ViewPort.TabIndex = 0;
            this.ViewPort.Paint += new System.Windows.Forms.PaintEventHandler(this.ViewPort_Paint);
            this.ViewPort.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ViewPort_MouseClick);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(96, 49);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(114, 20);
            this.tbName.TabIndex = 1;
            // 
            // tbWeight
            // 
            this.tbWeight.Location = new System.Drawing.Point(96, 92);
            this.tbWeight.Name = "tbWeight";
            this.tbWeight.Size = new System.Drawing.Size(114, 20);
            this.tbWeight.TabIndex = 2;
            // 
            // tbX
            // 
            this.tbX.Location = new System.Drawing.Point(96, 131);
            this.tbX.Name = "tbX";
            this.tbX.Size = new System.Drawing.Size(114, 20);
            this.tbX.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(55, 52);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name";
            // 
            // Weight
            // 
            this.Weight.AutoSize = true;
            this.Weight.Location = new System.Drawing.Point(49, 95);
            this.Weight.Name = "Weight";
            this.Weight.Size = new System.Drawing.Size(41, 13);
            this.Weight.TabIndex = 5;
            this.Weight.Text = "Weight";
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(76, 134);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(14, 13);
            this.lblX.TabIndex = 6;
            this.lblX.Text = "X";
            // 
            // tbY
            // 
            this.tbY.Location = new System.Drawing.Point(96, 170);
            this.tbY.Name = "tbY";
            this.tbY.Size = new System.Drawing.Size(114, 20);
            this.tbY.TabIndex = 7;
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(76, 173);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(14, 13);
            this.lblY.TabIndex = 8;
            this.lblY.Text = "Y";
            // 
            // tbStart
            // 
            this.tbStart.Location = new System.Drawing.Point(114, 431);
            this.tbStart.Name = "tbStart";
            this.tbStart.Size = new System.Drawing.Size(78, 20);
            this.tbStart.TabIndex = 9;
            // 
            // tbEnd
            // 
            this.tbEnd.Location = new System.Drawing.Point(114, 457);
            this.tbEnd.Name = "tbEnd";
            this.tbEnd.Size = new System.Drawing.Size(77, 20);
            this.tbEnd.TabIndex = 10;
            // 
            // blbFrom
            // 
            this.blbFrom.AutoSize = true;
            this.blbFrom.Location = new System.Drawing.Point(48, 431);
            this.blbFrom.Name = "blbFrom";
            this.blbFrom.Size = new System.Drawing.Size(33, 13);
            this.blbFrom.TabIndex = 11;
            this.blbFrom.Text = "From:";
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(48, 464);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(23, 13);
            this.lblTo.TabIndex = 12;
            this.lblTo.Text = "To:";
            // 
            // btnGreedy
            // 
            this.btnGreedy.Location = new System.Drawing.Point(12, 555);
            this.btnGreedy.Name = "btnGreedy";
            this.btnGreedy.Size = new System.Drawing.Size(79, 36);
            this.btnGreedy.TabIndex = 13;
            this.btnGreedy.Text = "Greedy Search";
            this.btnGreedy.UseVisualStyleBackColor = true;
            // 
            // btnShortestPath
            // 
            this.btnShortestPath.Location = new System.Drawing.Point(134, 555);
            this.btnShortestPath.Name = "btnShortestPath";
            this.btnShortestPath.Size = new System.Drawing.Size(76, 36);
            this.btnShortestPath.TabIndex = 14;
            this.btnShortestPath.Text = "Shortest Path";
            this.btnShortestPath.UseVisualStyleBackColor = true;
            // 
            // lblTo2nd
            // 
            this.lblTo2nd.AutoSize = true;
            this.lblTo2nd.Location = new System.Drawing.Point(48, 490);
            this.lblTo2nd.Name = "lblTo2nd";
            this.lblTo2nd.Size = new System.Drawing.Size(23, 13);
            this.lblTo2nd.TabIndex = 16;
            this.lblTo2nd.Text = "To:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(114, 483);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(77, 20);
            this.textBox1.TabIndex = 15;
            // 
            // lblEnd
            // 
            this.lblEnd.AutoSize = true;
            this.lblEnd.Location = new System.Drawing.Point(48, 516);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(29, 13);
            this.lblEnd.TabIndex = 18;
            this.lblEnd.Text = "End:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(114, 509);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(77, 20);
            this.textBox2.TabIndex = 17;
            // 
            // Path
            // 
            this.Path.FormattingEnabled = true;
            this.Path.Location = new System.Drawing.Point(277, 434);
            this.Path.Name = "Path";
            this.Path.Size = new System.Drawing.Size(325, 160);
            this.Path.TabIndex = 19;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(117, 209);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 20;
            this.btnAdd.Text = "Add Point";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 276);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "From:";
            // 
            // pathFrom
            // 
            this.pathFrom.Location = new System.Drawing.Point(52, 273);
            this.pathFrom.Name = "pathFrom";
            this.pathFrom.Size = new System.Drawing.Size(89, 20);
            this.pathFrom.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 316);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "From:";
            // 
            // pathTo
            // 
            this.pathTo.Location = new System.Drawing.Point(52, 313);
            this.pathTo.Name = "pathTo";
            this.pathTo.Size = new System.Drawing.Size(89, 20);
            this.pathTo.TabIndex = 23;
            // 
            // isTwoWay
            // 
            this.isTwoWay.AutoSize = true;
            this.isTwoWay.Location = new System.Drawing.Point(16, 349);
            this.isTwoWay.Name = "isTwoWay";
            this.isTwoWay.Size = new System.Drawing.Size(93, 17);
            this.isTwoWay.TabIndex = 25;
            this.isTwoWay.Text = "Two way path";
            this.isTwoWay.UseVisualStyleBackColor = true;
            // 
            // btnCreatePath
            // 
            this.btnCreatePath.Location = new System.Drawing.Point(162, 291);
            this.btnCreatePath.Name = "btnCreatePath";
            this.btnCreatePath.Size = new System.Drawing.Size(75, 26);
            this.btnCreatePath.TabIndex = 26;
            this.btnCreatePath.Text = "Create Path";
            this.btnCreatePath.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 611);
            this.Controls.Add(this.btnCreatePath);
            this.Controls.Add(this.isTwoWay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pathTo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pathFrom);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.Path);
            this.Controls.Add(this.lblEnd);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.lblTo2nd);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnShortestPath);
            this.Controls.Add(this.btnGreedy);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.blbFrom);
            this.Controls.Add(this.tbEnd);
            this.Controls.Add(this.tbStart);
            this.Controls.Add(this.lblY);
            this.Controls.Add(this.tbY);
            this.Controls.Add(this.lblX);
            this.Controls.Add(this.Weight);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.tbX);
            this.Controls.Add(this.tbWeight);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.ViewPort);
            this.Name = "Main";
            this.Text = "Main";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel ViewPort;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbWeight;
        private System.Windows.Forms.TextBox tbX;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label Weight;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.TextBox tbY;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.TextBox tbStart;
        private System.Windows.Forms.TextBox tbEnd;
        private System.Windows.Forms.Label blbFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Button btnGreedy;
        private System.Windows.Forms.Button btnShortestPath;
        private System.Windows.Forms.Label lblTo2nd;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblEnd;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ListBox Path;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pathFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox pathTo;
        private System.Windows.Forms.CheckBox isTwoWay;
        private System.Windows.Forms.Button btnCreatePath;
    }
}