﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data
{
    public class ShortestPathAlgorithm
    {
        string start, finish;
        public double distance = 0;
        Graph graph;
        List<string> nodesToVisit;
        public List<string> wholePath = new List<string>();

        public ShortestPathAlgorithm(Graph graph, List<string> nodes)
        {
            this.graph = graph;
            nodesToVisit = nodes;
        }
        public bool Search()
        {
            if (nodesToVisit.Count > 1)
            {
                start = nodesToVisit.ElementAt(0);
                finish = nodesToVisit.ElementAt(1);
            }

            if (!graph.graph.ContainsKey(start) || !graph.graph.ContainsKey(finish))
            {
                Console.WriteLine("one or more points not in graph, cant find path");
                return false;
            }

            List<Node> queue = new List<Node>();
            Node begining = graph.graph[start];
            Dictionary<string, string> testedNodes = new Dictionary<string, string>();

            foreach (var node in graph.graph.Values)
            {
                if (node == begining)
                {
                    begining.Distance = 0;
                }
                else
                {
                    node.Distance = double.PositiveInfinity;
                }
                queue.Add(node);

            }

            Node current = begining;
            while (queue.Count() > 0)
            {
                queue.Remove(current);
                if (current == graph.graph[finish])
                {
                    List<string> path = new List<string>();
                    distance += current.Distance;
                    while (testedNodes.ContainsKey(current.Name))
                    {
                        path.Insert(0, current.Name);
                        current = graph.graph[testedNodes[current.Name]];
                    }
                    path.Insert(0, start);
                    wholePath.AddRange(path);
                    //wholePath.ForEach(x => Console.WriteLine(x));
                    if (nodesToVisit.Count > 2)
                    {
                        nodesToVisit.RemoveAt(0);
                        Search();
                    }
                    else
                    {
                        Console.WriteLine(distance);
                        //distance = 0;
                    }
                    return true;


                }

                foreach (var p in current.paths)
                {
                    var distance = current.Distance + p.Length;
                    if (distance < p.EndPoint.Distance)
                    {
                        p.EndPoint.Distance = distance;
                        testedNodes[p.EndPoint.Name] = current.Name;
                    }
                }

                queue.Sort((x, y) => x.Distance.CompareTo(y.Distance));

                current = queue[0];


            }

            return false;
        }

    }
}
