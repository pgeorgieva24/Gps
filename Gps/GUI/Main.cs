﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Main : Form
    {
        int x, y;
        DialogProcessor dialogProcessor = new DialogProcessor();

        public Main()
        {
            InitializeComponent();
        }

        private void ViewPort_Paint(object sender, PaintEventArgs e)
        {
            dialogProcessor.g = e.Graphics;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string name = tbName.Text;
            double.TryParse(tbWeight.Text, out double Weight);
            int.TryParse(tbX.Text, out int X);
            int.TryParse(tbY.Text, out int Y);
            Node node = new Node(name, Weight, X, Y);

            dialogProcessor.DrawNode(node);
        }

        private void ViewPort_MouseClick(object sender, MouseEventArgs e)
        {
            Point p = new Point(e.X, e.Y);
            x = p.X;
            y = p.Y;
            tbX.Text = x.ToString();
            tbY.Text = y.ToString();
            ViewPort.Invalidate();
        }

    }
}
