﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class MyForm : Form
    {
        int x, y;
        DialogProcessor dialogProcessor = new DialogProcessor();
        public MyForm()
        {
            InitializeComponent();
        }
        private void ViewPort_Paint(object sender, PaintEventArgs e)
        {
            dialogProcessor.ReDraw(sender, e);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string name = tbName.Text;
            double.TryParse(tbWeight.Text, out double Weight);
            int.TryParse(tbX.Text, out int X);
            int.TryParse(tbY.Text, out int Y);
            Node node = new Node(name, X, Y, Weight);

            dialogProcessor.DrawNode(node);
            ViewPort.Invalidate();
        }

        private void btnCreatePath_Click(object sender, EventArgs e)
        {
            string from = pathFrom.Text;
            string to = pathTo.Text;
            double.TryParse(tbLength.Text, out double length);
            bool IsTwoWay = isTwoWay.Checked;
            if (IsTwoWay)
                dialogProcessor.CreateTwoWayPath(IsTwoWay, from, to, length);
            else dialogProcessor.CreatePath(from, to, length);

            ViewPort.Invalidate();
        }

        private void btnGreedy_Click(object sender, EventArgs e)
        {
            if (dialogProcessor.graph.graph.ContainsKey(tbStart.Text) && dialogProcessor.graph.graph.ContainsKey(tbEnd.Text))
            {
                bool isPathFound = GreedySearchAlgorithm.Search(tbStart.Text, tbEnd.Text, dialogProcessor.graph);
                Path.Items.Clear();
                foreach (var item in GreedySearchAlgorithm.path)
                {
                    Path.Items.Add(item + " -> ");
                }
                Path.Items.Add("Total distance: " + GreedySearchAlgorithm.distance);
                if (isPathFound) Path.Items.Add("Path Found!");
                else Path.Items.Add("No Path Found!");
            }
            else MessageBox.Show("Some nodes you entered are not in the graph :(");
        }

        private void btnShortestPath_Click(object sender, EventArgs e)
        {
            if (dialogProcessor.graph.graph.ContainsKey(tbStart.Text) && dialogProcessor.graph.graph.ContainsKey(tbEnd.Text))
            {
                List<string> nodes = new List<string> { tbStart.Text, tbEnd.Text };

                ShortestPathAlgorithm spa = new ShortestPathAlgorithm(dialogProcessor.graph, nodes);
                bool isPathFound = spa.Search();
                Path.Items.Clear();
                foreach (var item in spa.wholePath)
                {
                    Path.Items.Add(item + " -> ");
                }
                Path.Items.Add("Total distance: " + spa.distance);
                if (isPathFound) Path.Items.Add("Path Found!");
                else Path.Items.Add("No Path Found!");
            }
            else MessageBox.Show("Some nodes you entered are not in the graph :(");
        }

        private void btnSearchWith2Stops_Click(object sender, EventArgs e)
        {
            if (dialogProcessor.graph.graph.ContainsKey(tbOne.Text) && dialogProcessor.graph.graph.ContainsKey(tbTwo.Text)
                && dialogProcessor.graph.graph.ContainsKey(tbThree.Text) && dialogProcessor.graph.graph.ContainsKey(tbFour.Text))
            {
                List<string> nodes = new List<string> { tbOne.Text, tbTwo.Text, tbThree.Text, tbFour.Text };

                ShortestPathAlgorithm spa = new ShortestPathAlgorithm(dialogProcessor.graph, nodes);
                spa.Search();
                Path.Items.Clear();
                foreach (var item in spa.wholePath)
                {
                    Path.Items.Add(item + " -> ");
                }
                Path.Items.Add("Total distance: " + spa.distance);
                if (!(double.IsPositiveInfinity(spa.distance))) Path.Items.Add("Path Found!");
                else Path.Items.Add("No Path Found!");
            }
            else MessageBox.Show("Some nodes you entered are not in the graph :(");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                dialogProcessor.SaveAs(saveFileDialog.FileName);
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                dialogProcessor.OpenFile(openFileDialog.FileName);
                ViewPort.Invalidate();
            }
        }

        private void ViewPort_MouseClick(object sender, MouseEventArgs e)
        {
            Point p = new Point(e.X, e.Y);
            x = p.X;
            y = p.Y;
            tbX.Text = x.ToString();
            tbY.Text = y.ToString();
            ViewPort.Invalidate();
        }


    }
}
