﻿namespace GUI
{
    partial class MyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreatePath = new System.Windows.Forms.Button();
            this.isTwoWay = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pathTo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pathFrom = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.Path = new System.Windows.Forms.ListBox();
            this.lblEnd = new System.Windows.Forms.Label();
            this.tbEnd = new System.Windows.Forms.TextBox();
            this.lblTo2nd = new System.Windows.Forms.Label();
            this.tbThree = new System.Windows.Forms.TextBox();
            this.btnShortestPath = new System.Windows.Forms.Button();
            this.btnGreedy = new System.Windows.Forms.Button();
            this.lblTo = new System.Windows.Forms.Label();
            this.blbFrom = new System.Windows.Forms.Label();
            this.tbTwo = new System.Windows.Forms.TextBox();
            this.tbStart = new System.Windows.Forms.TextBox();
            this.lblY = new System.Windows.Forms.Label();
            this.tbY = new System.Windows.Forms.TextBox();
            this.lblX = new System.Windows.Forms.Label();
            this.Weight = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.tbX = new System.Windows.Forms.TextBox();
            this.tbWeight = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.ViewPort = new System.Windows.Forms.Panel();
            this.lblLength = new System.Windows.Forms.Label();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.btnSearchWith2Stops = new System.Windows.Forms.Button();
            this.gb = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbOne = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbFour = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.gb.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCreatePath
            // 
            this.btnCreatePath.Location = new System.Drawing.Point(68, 479);
            this.btnCreatePath.Name = "btnCreatePath";
            this.btnCreatePath.Size = new System.Drawing.Size(75, 26);
            this.btnCreatePath.TabIndex = 53;
            this.btnCreatePath.Text = "Create Path";
            this.btnCreatePath.UseVisualStyleBackColor = true;
            this.btnCreatePath.Click += new System.EventHandler(this.btnCreatePath_Click);
            // 
            // isTwoWay
            // 
            this.isTwoWay.AutoSize = true;
            this.isTwoWay.Location = new System.Drawing.Point(35, 442);
            this.isTwoWay.Name = "isTwoWay";
            this.isTwoWay.Size = new System.Drawing.Size(93, 17);
            this.isTwoWay.TabIndex = 52;
            this.isTwoWay.Text = "Two way path";
            this.isTwoWay.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 381);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 51;
            this.label2.Text = "То:";
            // 
            // pathTo
            // 
            this.pathTo.Location = new System.Drawing.Point(61, 378);
            this.pathTo.Name = "pathTo";
            this.pathTo.Size = new System.Drawing.Size(89, 20);
            this.pathTo.TabIndex = 50;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 355);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 49;
            this.label1.Text = "From:";
            // 
            // pathFrom
            // 
            this.pathFrom.Location = new System.Drawing.Point(62, 352);
            this.pathFrom.Name = "pathFrom";
            this.pathFrom.Size = new System.Drawing.Size(89, 20);
            this.pathFrom.TabIndex = 48;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(66, 231);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 47;
            this.btnAdd.Text = "Add Point";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // Path
            // 
            this.Path.FormattingEnabled = true;
            this.Path.Location = new System.Drawing.Point(800, 508);
            this.Path.Name = "Path";
            this.Path.Size = new System.Drawing.Size(325, 108);
            this.Path.TabIndex = 46;
            // 
            // lblEnd
            // 
            this.lblEnd.AutoSize = true;
            this.lblEnd.Location = new System.Drawing.Point(54, 92);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(29, 13);
            this.lblEnd.TabIndex = 45;
            this.lblEnd.Text = "End:";
            // 
            // tbEnd
            // 
            this.tbEnd.Location = new System.Drawing.Point(89, 89);
            this.tbEnd.Name = "tbEnd";
            this.tbEnd.Size = new System.Drawing.Size(77, 20);
            this.tbEnd.TabIndex = 44;
            // 
            // lblTo2nd
            // 
            this.lblTo2nd.AutoSize = true;
            this.lblTo2nd.Location = new System.Drawing.Point(61, 104);
            this.lblTo2nd.Name = "lblTo2nd";
            this.lblTo2nd.Size = new System.Drawing.Size(23, 13);
            this.lblTo2nd.TabIndex = 43;
            this.lblTo2nd.Text = "To:";
            // 
            // tbThree
            // 
            this.tbThree.Location = new System.Drawing.Point(90, 101);
            this.tbThree.Name = "tbThree";
            this.tbThree.Size = new System.Drawing.Size(77, 20);
            this.tbThree.TabIndex = 42;
            // 
            // btnShortestPath
            // 
            this.btnShortestPath.Location = new System.Drawing.Point(131, 144);
            this.btnShortestPath.Name = "btnShortestPath";
            this.btnShortestPath.Size = new System.Drawing.Size(75, 35);
            this.btnShortestPath.TabIndex = 41;
            this.btnShortestPath.Text = "Shortest Path";
            this.btnShortestPath.UseVisualStyleBackColor = true;
            this.btnShortestPath.Click += new System.EventHandler(this.btnShortestPath_Click);
            // 
            // btnGreedy
            // 
            this.btnGreedy.Location = new System.Drawing.Point(22, 144);
            this.btnGreedy.Name = "btnGreedy";
            this.btnGreedy.Size = new System.Drawing.Size(84, 35);
            this.btnGreedy.TabIndex = 40;
            this.btnGreedy.Text = "Greedy Search";
            this.btnGreedy.UseVisualStyleBackColor = true;
            this.btnGreedy.Click += new System.EventHandler(this.btnGreedy_Click);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(61, 74);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(23, 13);
            this.lblTo.TabIndex = 39;
            this.lblTo.Text = "To:";
            // 
            // blbFrom
            // 
            this.blbFrom.AutoSize = true;
            this.blbFrom.Location = new System.Drawing.Point(50, 54);
            this.blbFrom.Name = "blbFrom";
            this.blbFrom.Size = new System.Drawing.Size(33, 13);
            this.blbFrom.TabIndex = 38;
            this.blbFrom.Text = "From:";
            // 
            // tbTwo
            // 
            this.tbTwo.Location = new System.Drawing.Point(90, 71);
            this.tbTwo.Name = "tbTwo";
            this.tbTwo.Size = new System.Drawing.Size(77, 20);
            this.tbTwo.TabIndex = 37;
            // 
            // tbStart
            // 
            this.tbStart.Location = new System.Drawing.Point(89, 51);
            this.tbStart.Name = "tbStart";
            this.tbStart.Size = new System.Drawing.Size(78, 20);
            this.tbStart.TabIndex = 36;
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(42, 196);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(14, 13);
            this.lblY.TabIndex = 35;
            this.lblY.Text = "Y";
            // 
            // tbY
            // 
            this.tbY.Location = new System.Drawing.Point(62, 193);
            this.tbY.Name = "tbY";
            this.tbY.Size = new System.Drawing.Size(88, 20);
            this.tbY.TabIndex = 34;
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(41, 170);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(14, 13);
            this.lblX.TabIndex = 33;
            this.lblX.Text = "X";
            // 
            // Weight
            // 
            this.Weight.AutoSize = true;
            this.Weight.Location = new System.Drawing.Point(13, 144);
            this.Weight.Name = "Weight";
            this.Weight.Size = new System.Drawing.Size(41, 13);
            this.Weight.TabIndex = 32;
            this.Weight.Text = "Weight";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(20, 118);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 31;
            this.lblName.Text = "Name";
            // 
            // tbX
            // 
            this.tbX.Location = new System.Drawing.Point(61, 167);
            this.tbX.Name = "tbX";
            this.tbX.Size = new System.Drawing.Size(88, 20);
            this.tbX.TabIndex = 30;
            // 
            // tbWeight
            // 
            this.tbWeight.Location = new System.Drawing.Point(60, 141);
            this.tbWeight.Name = "tbWeight";
            this.tbWeight.Size = new System.Drawing.Size(88, 20);
            this.tbWeight.TabIndex = 29;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(61, 115);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(88, 20);
            this.tbName.TabIndex = 28;
            // 
            // ViewPort
            // 
            this.ViewPort.Location = new System.Drawing.Point(165, 12);
            this.ViewPort.Name = "ViewPort";
            this.ViewPort.Size = new System.Drawing.Size(626, 602);
            this.ViewPort.TabIndex = 27;
            this.ViewPort.Paint += new System.Windows.Forms.PaintEventHandler(this.ViewPort_Paint);
            this.ViewPort.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ViewPort_MouseClick);
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(12, 407);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(43, 13);
            this.lblLength.TabIndex = 55;
            this.lblLength.Text = "Length:";
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(61, 404);
            this.tbLength.Name = "tbLength";
            this.tbLength.Size = new System.Drawing.Size(89, 20);
            this.tbLength.TabIndex = 54;
            // 
            // btnSearchWith2Stops
            // 
            this.btnSearchWith2Stops.Location = new System.Drawing.Point(73, 170);
            this.btnSearchWith2Stops.Name = "btnSearchWith2Stops";
            this.btnSearchWith2Stops.Size = new System.Drawing.Size(83, 36);
            this.btnSearchWith2Stops.TabIndex = 56;
            this.btnSearchWith2Stops.Text = "Search With 2 Stops ";
            this.btnSearchWith2Stops.UseVisualStyleBackColor = true;
            this.btnSearchWith2Stops.Click += new System.EventHandler(this.btnSearchWith2Stops_Click);
            // 
            // gb
            // 
            this.gb.Controls.Add(this.tbStart);
            this.gb.Controls.Add(this.blbFrom);
            this.gb.Controls.Add(this.btnGreedy);
            this.gb.Controls.Add(this.btnShortestPath);
            this.gb.Controls.Add(this.tbEnd);
            this.gb.Controls.Add(this.lblEnd);
            this.gb.Location = new System.Drawing.Point(859, 20);
            this.gb.Name = "gb";
            this.gb.Size = new System.Drawing.Size(225, 236);
            this.gb.TabIndex = 57;
            this.gb.TabStop = false;
            this.gb.Text = "Search by Two Points";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbOne);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbFour);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnSearchWith2Stops);
            this.groupBox1.Controls.Add(this.tbTwo);
            this.groupBox1.Controls.Add(this.lblTo);
            this.groupBox1.Controls.Add(this.tbThree);
            this.groupBox1.Controls.Add(this.lblTo2nd);
            this.groupBox1.Location = new System.Drawing.Point(859, 274);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(225, 228);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search With Two Stops";
            // 
            // tbOne
            // 
            this.tbOne.Location = new System.Drawing.Point(90, 45);
            this.tbOne.Name = "tbOne";
            this.tbOne.Size = new System.Drawing.Size(78, 20);
            this.tbOne.TabIndex = 57;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 58;
            this.label3.Text = "From:";
            // 
            // tbFour
            // 
            this.tbFour.Location = new System.Drawing.Point(91, 127);
            this.tbFour.Name = "tbFour";
            this.tbFour.Size = new System.Drawing.Size(77, 20);
            this.tbFour.TabIndex = 59;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 60;
            this.label4.Text = "End:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 61;
            this.btnSave.Text = "Save As";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(12, 41);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 23);
            this.btnOpen.TabIndex = 62;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // MyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1137, 626);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gb);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.btnCreatePath);
            this.Controls.Add(this.isTwoWay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pathTo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pathFrom);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.Path);
            this.Controls.Add(this.lblY);
            this.Controls.Add(this.tbY);
            this.Controls.Add(this.lblX);
            this.Controls.Add(this.Weight);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.tbX);
            this.Controls.Add(this.tbWeight);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.ViewPort);
            this.Name = "MyForm";
            this.Text = "Form";
            this.gb.ResumeLayout(false);
            this.gb.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreatePath;
        private System.Windows.Forms.CheckBox isTwoWay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox pathTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pathFrom;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox Path;
        private System.Windows.Forms.Label lblEnd;
        private System.Windows.Forms.TextBox tbEnd;
        private System.Windows.Forms.Label lblTo2nd;
        private System.Windows.Forms.TextBox tbThree;
        private System.Windows.Forms.Button btnShortestPath;
        private System.Windows.Forms.Button btnGreedy;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label blbFrom;
        private System.Windows.Forms.TextBox tbTwo;
        private System.Windows.Forms.TextBox tbStart;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.TextBox tbY;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label Weight;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbX;
        private System.Windows.Forms.TextBox tbWeight;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Panel ViewPort;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Button btnSearchWith2Stops;
        private System.Windows.Forms.GroupBox gb;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbOne;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbFour;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}